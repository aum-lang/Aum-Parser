extern crate aum_parser;

use aum_parser::types;
use aum_parser::types::Type;

#[test]
fn test_mut_string() {
    let ident = "test";
    let value = "so long, and thanks for all the fish";

    let new_mut_str = types::MutString::new(ident, value);

    assert_eq!(new_mut_str.get_ident(), ident);
    assert_eq!(new_mut_str.get_value(), value);
}

#[test]
fn test_int() {
    let ident = "test";
    let value = 42;

    let new_int = types::Integer::new(ident, value);

    assert_eq!(new_int.get_ident(), ident);
    assert_eq!(new_int.get_value(), value);
}
