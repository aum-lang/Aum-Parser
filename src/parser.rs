use aum_lexer::tokens::TokenType;
use generator;
use statement::Statement;

/// Used to signify to the Parser object what form of code to generate
enum ParserMode {
    x86,
    CXX,
}

/// Parser converts Vec of TokenType into
pub struct Parser {
    Tokens: Vec<TokenType>,
    Mode: ParserMode,
}

impl Parser {
    /// Creates a new Parser
    fn new(tokens: Vec<TokenType>, mode: ParserMode) -> Parser {
        Parser {
            Tokens: tokens,
            Mode: mode,
        }
    }

    /// Creates a new Parser in x86 assembly mode
    pub fn new_x86(tokens: Vec<TokenType>) -> Parser {
        Parser::new(tokens, ParserMode::x86)
    }

    /// Creates a new Parser in C++14 assembly mode
    pub fn new_CXX(tokens: Vec<TokenType>) -> Parser {
        Parser::new(tokens, ParserMode::CXX)
    }

    /// Parses the tokens passed during Parser creation into the appropriate Aum statement objects
    ///
    /// # Returns
    ///
    /// vec of boxed Aum statements
    pub fn parse(&self) -> Vec<Box<Statement>> {
        let statements: Vec<Box<Statement>> = Vec::new();

        let token_iter = self.Tokens.iter().peekable();

        while token_iter.peek() != None {
            // Retrieve next token in sequence
            let cur_token = token_iter.next().unwrap();

            let statement: Option<Box<Statement>> = None;

            match cur_token {
                TokenType::EndOfFile => break,
                TokenType::Illegal => panic!("Illegal control token!"),
                TokenType::Ident(ident) => {
                    // TODO: Make this handle missing values better
                    // There must be a following value, otherwise please blow up
                    let next_token = token_iter.next().unwrap();
                    if *next_token != TokenType::Initialize {
                         panic!("Unexpected token {:?}!", cur_token)
                    }
                },
                _ => panic!("Not implemented!"),
            };
        }

        // Return generated statements
        statements
    }

    /// Converts Aum statements into appropriate output language strings before combining them into a single string
    /// that can be output to a file
    ///
    /// # Arguments
    ///
    /// - `statements` - vec of boxed Aum statements
    ///
    /// # Returns
    ///
    /// str of generated code to be output
    pub fn to_source(&self, statements: Vec<Box<Statement>>) -> &'static str {
        // Convert Aum statements to appropriate source statements
        let generated = match self.Mode {
            ParserMode::CXX => {
                generator::generate_cxx(statements);
            }
            ParserMode::x86 => {
                generator::generate_x86(statements);
            }
        };

        // Will hold combined strings to return
        let result = "";

        result
    }
}
