use std::{fmt, string};

/// Aum basic types
pub trait Type: fmt::Debug {
    type ItemType;

    fn get_value(&self) -> Self::ItemType;
    fn get_ident(&self) -> &str;
    fn as_str(&self) -> &'static str;
}

// /// Mutable string
// #[derive(Debug)]
// pub struct MutString<'val> {
//     pub ident: &'val str,
//     pub value: &'val str,
// }

// impl<'val> MutString<'val> {
//     pub fn new(ident: &'val str, val: &'val str) -> MutString<'val> {
//         MutString {
//             ident: ident,
//             value: val,
//         }
//     }
// }

// impl<'val> Type for MutString<'val> {
//     type ItemType = String;

//     fn get_value(&self) -> Self::ItemType {
//         self.value.into()
//     }

//     fn get_ident(&self) -> &str {
//         self.ident.clone()
//     }
// }

// impl<'val> string::ToString for MutString<'val> {
//     fn to_string(&self) -> String {
//         format!("{}: {}", self.ident, self.value)
//     }
// }

/// Integer type. Currently is 32 bit integer, will add other numeric types later
#[derive(Debug)]
pub struct Integer<'val> {
    ident: &'val str,
    value: i32,
}

impl<'val> Integer<'val> {
    pub fn new(ident: &'val str, val: i32) -> Integer<'val> {
        Integer {
            ident: ident,
            value: val,
        }
    }
}

impl<'val> Type for Integer<'val> {
    type ItemType = i32;

    fn get_value(&self) -> Self::ItemType {
        self.value.clone()
    }

    fn get_ident(&self) -> &str {
        self.ident.clone()
    }

    fn as_str(&self) -> &'static str {
        self.value.to_string().as_str()
    }
}

impl<'val> string::ToString for Integer<'val> {
    fn to_string(&self) -> String {
        format!("{}: {}", self.ident, self.value.to_string())
    }
}
